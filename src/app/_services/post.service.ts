import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PostService {
  private API_ENDPOINT = 'https://jtar71dvoi.execute-api.us-east-1.amazonaws.com/Production/lambdatest';
  constructor(private _http: Http) {}
  
  saveName(name: any) {
    let headers = new Headers({ 'Content-Type': 'application/json' }); // create new Headers object with header Content-Type is application/json.
    // headers.append('Authorization', 'Bearer ' + your_token); //JWT token
    let options = new RequestOptions({ headers: headers });
    
    return this._http.post(this.API_ENDPOINT, JSON.stringify({name: name}), options)
      .map(res => res);
  }
}