import {Component} from '@angular/core';
import {PostService} from './_services/post.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'Toothless';
    model: any = {};
    postResponse: any;
    private _sub;

    constructor(private postService: PostService) {

    }
    sendRequest() {
        this._sub = this.postService.saveName(this.model.name)
            .subscribe(data => {
                console.log(data);
                this.model.postResponse = data;
                this.model.postResponseBody = data['_body'];
            });
    }
}
