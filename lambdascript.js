const AWS = require('aws-sdk');
const querystring = require('querystring');
const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});


 
exports.handler = (event, context, callback) => {
    var body = JSON.parse(event.body);
    dynamodb.putItem({
        TableName: "lambdaTest",
        Item: {
            "name": {
                S: body.name
            }
        }
    }, function(err, data) {
        if (err) {
            console.log(err, err.stack);
            callback(null, {
                statusCode: '500',
                body: err,
                headers: {
                    "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                    "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                  },
            });
        } else {
            callback(null, {
                statusCode: '200',
                body: 'Hello ' + body.name + '!',
                headers: {
                    "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                    "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                  },
            });
        }
    })
};
